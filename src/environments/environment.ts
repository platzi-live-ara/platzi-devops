// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCeNHCyd8zDckdJ34g0FdzI4a9ofyjkS7c",
    authDomain: "tarjetacredito-bbf8d.firebaseapp.com",
    projectId: "tarjetacredito-bbf8d",
    storageBucket: "tarjetacredito-bbf8d.appspot.com",
    messagingSenderId: "122275774591",
    appId: "1:122275774591:web:e84e8e4f9ee20132dcff4a"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
