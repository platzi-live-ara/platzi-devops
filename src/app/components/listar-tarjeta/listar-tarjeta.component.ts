import { Component, OnInit } from '@angular/core';
import { TarjetaService } from 'src/app/services/tarjeta.service';
import { TarjetaCreditoModel } from '../models/TarjetaCredito';

@Component({
  selector: 'app-listar-tarjeta',
  templateUrl: './listar-tarjeta.component.html',
  styleUrls: ['./listar-tarjeta.component.scss']
})
export class ListarTarjetaComponent implements OnInit {

  public listarTarjeta: Array<TarjetaCreditoModel> = [];

  constructor(private tarjetaService: TarjetaService) { }

  ngOnInit(): void {
    this.getTarjetas();
  }

  getTarjetas(){
    this.tarjetaService.getTarjetas().subscribe(res => {
      this.listarTarjeta = [];
      res.forEach((element: any) => {
        this.listarTarjeta.push({
          id: element.payload.doc.id,
          ...element.payload.doc.data()
        })
      });
    })
  }

  deleteTarjeta(id: any){
    this.tarjetaService.eliminarTarjeta(id).then(() => {

    }, error => {

    })
  }

  editarTarjeta(tarjeta: TarjetaCreditoModel){
    this.tarjetaService.addTarjetaEdit(tarjeta);
  }

}
