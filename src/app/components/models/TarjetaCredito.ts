export class TarjetaCreditoModel{
    id?: string;
    titular: string;
    numeroTarjeta: string;
    fechaExpiracion: string;
    cvv: number;
    fechaCreacion?: Date;
    fechaActualizacion: Date;
}